import argparse
import time
import datetime
import sys
sys.path = ['../../'] + sys.path
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import logging
from baselines.common.distributions import make_pdtype
from baselines import logger, bench
from baselines.common.misc_util import (
    set_global_seeds,
    boolean_flag,
)
from baselines.common.vec_env.subproc_vec_env import SubprocVecEnv
import training as training
from models import Actor, Critic, MetaActor, ResActor, ResCritic
from memory import Memory, MultiprocessMemory
from noise import *
from osim.env import ProstheticsEnv
from visualize import Visualizer
import gym
import tensorflow as tf
from policies.ddpg import DDPG
from policies.ppo import PPO
from env import make_env, get_obs_length
from util import get_difficulty
import baselines.common.tf_util as U

from observation import ObsProcessWrapper, RewardReshapeWrapper

algorithm_with_name = {'DDPG': DDPG, 'PPO': PPO}
activation_with_name = {'relu': tf.nn.relu, 'selu': tf.nn.selu, 'elu': tf.nn.elu}

def run(alias, run_name, algorithm, num_process, seed, skip_frame, evaluation, batch_size, layer_norm, activation, layer_num, layer_width, memory_size, model_dir, meta, residual, clip_factor, res_activation, **kwargs):
    obs_length = get_obs_length(**kwargs)
    policy = algorithm_with_name[algorithm]
    # Create envs.
    gym.logger.setLevel(logging.ERROR)
    env = [make_env(i, seed, skip_frame, logger.get_dir(), **kwargs) for i in range(num_process)]
    env = SubprocVecEnv(env)

    assert len(clip_factor) == 3

    def make_eval_env():
        def callable():
            if evaluation:
                eval_env = ProstheticsEnv(visualize=True, difficulty=get_difficulty(**kwargs))
            else:
                eval_env = ProstheticsEnv(visualize=False, difficulty=get_difficulty(**kwargs))
            if kwargs['obs_process']:
                eval_env = ObsProcessWrapper(eval_env, **kwargs)
            # eval_env = bench.Monitor(eval_env, os.path.join(logger.get_dir(), 'eval'))
            return eval_env
        return callable
    eval_env = [make_eval_env() for i in range(4)]
    eval_env = SubprocVecEnv(eval_env)

    # initial perturbation
    eval_env.reset()
    for _ in range(25):
        _, _, done, info = eval_env.step(np.random.rand(4, 19))

    # Configure components.
    nb_actions = env.action_space.shape[-1]
    if meta:
        action_shape = (len(model_dir),)
    else:
        action_shape = env.action_space.shape
    if algorithm in ['DDPG']:
        memory = Memory(limit=int(memory_size), action_shape=action_shape, observation_shape=(obs_length,))
        critic = Critic(layer_norm=layer_norm, withaction=True, activation=activation, layer_num=layer_num, layer_width=layer_width)
        actor = Actor(nb_actions, layer_norm=layer_norm, activation=activation, layer_num=layer_num, layer_width=layer_width)
    elif algorithm in ['PPO']:
        memory = MultiprocessMemory(num_process, limit=int(2e4), action_shape=env.action_space.shape, observation_shape=(160,))
        critic = Critic(layer_norm=layer_norm, withaction=False, activation=activation)
        actor = Actor(nb_actions, layer_norm=layer_norm, distribution=make_pdtype(env.action_space), activation=activation)

    visualizer = Visualizer(alias, run_name)

    # Seed everything to make things reproducible.
    tf.reset_default_graph()
    set_global_seeds(seed)

    # Set Algo

    print('model dir:', model_dir)
    agents = []
    if meta:
        for dir in model_dir:
            graph_best = tf.Graph()
            with graph_best.as_default():
                sess_best = U.single_threaded_session()
                new_saver = tf.train.import_meta_graph(dir + '.meta')
                new_saver.restore(sess_best, dir)
                ops = tf.get_collection('eval')
                agents.append((sess_best, ops))

    print('agent length:', len(agents))
    if meta:
        meta_actor = MetaActor(len(agents), layer_norm=layer_norm, activation=activation, layer_num=1, layer_width=64, name='meta_actor')
        meta_critic = Critic(layer_norm=layer_norm, withaction=True, activation=activation, layer_num=1, layer_width=64, name='meta_critic')
        agent = policy(meta_actor, meta_critic, memory, (obs_length, ), (len(agents), ))
    elif residual:
        actor = ResActor(nb_actions, clip_factor=clip_factor, res_activation=res_activation, layer_norm=layer_norm, activation=activation, layer_num=layer_num, layer_width=layer_width)
        critic = ResCritic(clip_factor=clip_factor, res_activation=res_activation, layer_norm=layer_norm, withaction=True, activation=activation, layer_num=layer_num, layer_width=layer_width)
        agent = policy(actor, critic, memory, (obs_length,), env.action_space.shape)
    else:
        agent = policy(actor, critic, memory, (obs_length,), env.action_space.shape)

    # Disable logging for rank != 0 to avoid noise.
    start_time = time.time()
    training.train(env=env, agent=agent, agents=agents, num_process=num_process, eval_env=eval_env,
        memory=memory, visualizer=visualizer, batch_size=batch_size, model_dir=model_dir, meta=meta, residual=residual, **kwargs)
    env.close()
    if eval_env is not None:
        eval_env.close()
    logger.info('total runtime: {}s'.format(time.time() - start_time))


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--round', type=int, default=1)
    parser.add_argument('--alias', type=str, default='youralias')
    parser.add_argument('--algorithm', choices=algorithm_with_name.keys(), default='DDPG')
    parser.add_argument('--seed', help='RNG seed', type=int, default=0)
    parser.add_argument('--skip-frame', type=int, default=2)
    boolean_flag(parser, 'popart', default=False)
    parser.add_argument('--nb-epochs', type=int, default=1000)
    parser.add_argument('--nb-epoch-cycles', type=int, default=20)
    parser.add_argument('--nb-eval-steps', type=int, default=10000) # till episode end
    parser.add_argument('--activation', choices=activation_with_name.keys(), default='selu')
    parser.add_argument('--layer-num', type=int, default=2)
    parser.add_argument('--layer-width', type=int, default=64)
    parser.add_argument('--memory-size', type=int, default=1e6)
    parser.add_argument('--model-dir', nargs='+', type=str)

    bool_mapper = lambda str: True if 'True' in str or 'true' in str else False
    parser.add_argument('--reward-process', type=bool_mapper, default=True)
    parser.add_argument('--reward-bonus', type=float, default=1.0)
    parser.add_argument('--obs-process', type=bool_mapper, default=True)
    parser.add_argument('--add-feature', type=bool_mapper, default=True)
    parser.add_argument('--old-version', type=bool_mapper, default=False)
    parser.add_argument('--y-axis', type=bool_mapper, default=False)
    parser.add_argument('--meta', type=bool_mapper, default=False)
    parser.add_argument('--residual', type=bool_mapper, default=False)
    parser.add_argument('--clip-factor', nargs='+', type=float)
    parser.add_argument('--res-activation', type=str, default='tanh')

    boolean_flag(parser, 'evaluation', default=False)
    boolean_flag(parser, 'HPC', default=False)
    args, _ = parser.parse_known_args()
    D = algorithm_with_name[args.algorithm].DEFAULTS

    parser.add_argument('--num-process', type=int, default=D['num-process'])
    parser.add_argument('--run-name', type=str, default=D['run-name'])
    parser.add_argument('--layer-norm', type=str, default=D['layer-norm'])
    parser.add_argument('--batch-size', type=int, default=D['batch-size'])
    parser.add_argument('--nb-train-steps', type=int, default=D['nb-train-steps'])
    parser.add_argument('--nb-rollout-steps', type=int, default=D['nb-rollout-steps'])

    args, _ = parser.parse_known_args()
    args.activation = activation_with_name[args.activation]
    dict_args = vars(args)
    return dict_args, _

if __name__ == '__main__':
    args, _ = parse_args()
    cwd = os.path.join(os.getcwd(), 'log')
    args['run_name'] += datetime.datetime.now().strftime("-%Y-%m-%d-%H-%M-%S") + '-' + str(args['seed'])
    cwd = os.path.join(cwd, args['run_name'])
    os.makedirs(cwd, exist_ok=True)
    logger.configure(dir=cwd)
    logger.info('Command Line: ' + str(sys.argv))
    logger.info(str(_) + " is ignored by main parser")
    # Run actual script.
    run(**args)
