import gym
from osim.env import ProstheticsEnv
from baselines import logger, bench
import os
from observation import ObsProcessWrapper, RewardReshapeWrapper
from util import get_difficulty

def get_obs_length(obs_process, old_version, **kwargs):
    env = ProstheticsEnv(visualize=False, difficulty=get_difficulty(old_version=old_version, **kwargs))
    if obs_process:
        env = ObsProcessWrapper(env, old_version=old_version, **kwargs)
    index = 0 if old_version else 1
    result = len(env.reset()[index])
    return result

class FinalObsWrapper(gym.Wrapper):
    def __init__(self, env):
        super(FinalObsWrapper, self).__init__(env)

    def step(self, action):
        obs, r, done, info = self.env.step(action)
        # since SubprocVecEnv will automatically reset env after it is done, 
        # we have to save the final obs with info 
        return obs, r, done, {'obs': obs}

    def reset(self):
        return self.env.reset()

class SkipframeWrapper(gym.Wrapper):
    def __init__(self, env, skipcnt):
        super(SkipframeWrapper, self).__init__(env)
        self.skipcnt = skipcnt

    def step(self, action):
        tr = 0.
        for i in range(self.skipcnt):
            obs, r, done, info = self.env.step(action)
            tr += r
            if done:
                break
        return obs, tr, done, info

    def reset(self):
        return self.env.reset()

def make_env(rank, seed, skipcnt, log_dir=None, reward_process=False, reward_bonus=1.0, obs_process=True, add_feature=True, round=1, **kwargs):
    def _thunk():
        env = ProstheticsEnv(visualize=False, difficulty=get_difficulty(round))
        if obs_process:
            env = ObsProcessWrapper(env, add_feature, round, **kwargs)
        if reward_process:
            env = RewardReshapeWrapper(env, reward_bonus)
        # ProstheticsEnv have no seed
        #env.seed(seed + 100000 * rank)
        env = FinalObsWrapper(env)
        env = SkipframeWrapper(env, skipcnt)
        env = bench.Monitor(env, os.path.join(log_dir, str(rank)))
        return env
    return _thunk
